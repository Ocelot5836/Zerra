#version 330

in vec4 pass_Position;
in vec2 pass_TextureCoords;

out vec4 out_Color;

uniform sampler2D texture;
uniform vec4 size;
uniform vec4 textureCoords;

void main(void) {
	float x = pass_Position.x - size.x;
	float y = pass_Position.y - size.y;
	float u = (textureCoords.x / textureCoords.z) * size.z;
	float v = (textureCoords.y / textureCoords.w) * size.w;
	out_Color = texture(texture, pass_TextureCoords);
}