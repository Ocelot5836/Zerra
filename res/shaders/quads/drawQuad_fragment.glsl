#version 330

in vec4 pass_Position;

out vec4 out_Color;

uniform vec4 size;
uniform vec3 color1;
uniform vec3 color2;

const float bias = 0.1f;

void main(void) {
	float x = pass_Position.x - size.x;
	float y = pass_Position.y - size.y;
	float xy = (x / size.z + bias) * (1 - y / size.w + bias);
	out_Color = vec4(mix(color1, color2, clamp(xy, 0.0, 1.0)), 1.0);
}