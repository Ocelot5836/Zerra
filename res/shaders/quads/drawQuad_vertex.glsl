#version 140

in vec3 position;

out vec4 pass_Position;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

void main(void) {
	gl_Position = projectionMatrix * viewMatrix * transformationMatrix * vec4(position, 1.0);
	pass_Position = transformationMatrix * vec4(position, 1.0);
}