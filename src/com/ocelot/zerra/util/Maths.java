package com.ocelot.zerra.util;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import com.ocelot.zerra.entities.Camera;

public class Maths {

	/**
	 * Creates a 2d transformation matrix.
	 * 
	 * @param x
	 *            The translation in the x axis
	 * @param y
	 *            The translation in the y axis
	 * @param rotationX
	 *            The rotation in the x axis
	 * @param rotationY
	 *            The rotation in the y axis
	 * @param scaleX
	 *            The scale in the x axis
	 * @param scaleY
	 *            The scale in the y axis
	 * @return The matrix created with all the data stored inside
	 */
	public static Matrix4f createTransformationMatrix(float x, float y, float rotationX, float rotationY, float scaleX, float scaleY) {
		return createTransformationMatrix(x, y, 0, rotationX, rotationY, 0, scaleX, scaleY, 0);
	}

	/**
	 * Creates a 3d transformation matrix.
	 * 
	 * @param x
	 *            The translation in the x axis
	 * @param y
	 *            The translation in the y axis
	 * @param z
	 *            The translation in the z axis
	 * @param rotationX
	 *            The rotation in the x axis
	 * @param rotationY
	 *            The rotation in the y axis
	 * @param rotationZ
	 *            The rotation in the z axis
	 * @param scaleX
	 *            The scale in the x axis
	 * @param scaleY
	 *            The scale in the y axis
	 * @param scaleZ
	 *            The scale in the z axis
	 * @return The matrix created with all the data stored inside
	 */
	public static Matrix4f createTransformationMatrix(float x, float y, float z, float rotationX, float rotationY, float rotationZ, float scaleX, float scaleY, float scaleZ) {
		Matrix4f matrix = new Matrix4f();
		matrix.setIdentity();
		Matrix4f.translate(new Vector3f(x, y, z), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotationX), new Vector3f(1, 0, 0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotationY), new Vector3f(0, 1, 0), matrix, matrix);
		Matrix4f.rotate((float) Math.toRadians(rotationZ), new Vector3f(0, 0, 1), matrix, matrix);
		Matrix4f.scale(new Vector3f(scaleX, scaleY, scaleZ), matrix, matrix);
		return matrix;
	}

	public static Matrix4f createViewMatrix(Camera camera) {
		Matrix4f viewMatrix = new Matrix4f();
		Vector3f cameraPos = camera.getPosition();
		Vector3f negativeCameraPos = new Vector3f(-cameraPos.x, -cameraPos.y, -cameraPos.z);
		viewMatrix.setIdentity();
		Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix, viewMatrix);
		Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
		Matrix4f.rotate((float) Math.toRadians(camera.getRoll()), new Vector3f(0, 0, 1), viewMatrix, viewMatrix);
		Matrix4f.translate(negativeCameraPos, viewMatrix, viewMatrix);
		return viewMatrix;
	}
}