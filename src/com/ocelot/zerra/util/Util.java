package com.ocelot.zerra.util;

import java.util.List;
import java.util.Locale;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Some misc utilities that can be accessed.
 * 
 * @author Ocelot5836
 */
public class Util {

	/**
	 * @return The current OS type
	 */
	public static EnumOS getOSType() {
		String s = System.getProperty("os.name").toLowerCase(Locale.ROOT);

		if (s.contains("win")) {
			return Util.EnumOS.WINDOWS;
		} else if (s.contains("mac")) {
			return Util.EnumOS.OSX;
		} else if (s.contains("solaris")) {
			return Util.EnumOS.SOLARIS;
		} else if (s.contains("sunos")) {
			return Util.EnumOS.SOLARIS;
		} else if (s.contains("linux")) {
			return Util.EnumOS.LINUX;
		} else {
			return s.contains("unix") ? Util.EnumOS.LINUX : Util.EnumOS.UNKNOWN;
		}
	}

	/**
	 * Returns the last element in the list.
	 * 
	 * @param list
	 *            The list to get the element from
	 * @return The last element, null if the size is less than or equal to zero
	 */
	public static <T> T getLastElement(List<T> list) {
		return list.isEmpty() || list.size() <= 0 ? null : list.get(list.size() - 1);
	}

	/**
	 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
	 * 
	 * <br>
	 * </br>
	 * 
	 * The OS types that are recognized.
	 * 
	 * @author Ocelot5836
	 */
	public enum EnumOS {
		LINUX, SOLARIS, WINDOWS, OSX, UNKNOWN;
	}
}