package com.ocelot.zerra.gfx;

import org.lwjgl.util.vector.Matrix4f;

import com.ocelot.zerra.entities.Camera;
import com.ocelot.zerra.gfx.shaders.Draw3dShader;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Draws 3D objects onto the screen.
 * 
 * @author Ocelot5836
 */
public class Draw3D {

	/** The shaders used in default rendering. */

	private static Draw3dShader defaultShader = new Draw3dShader();

	/**
	 * Initalizes the renderer.
	 */
	public static void init() {
		defaultShader.start();
		defaultShader.loadProjectionMatrix(Renderer.getProjectionMatrix());
		defaultShader.stop();
	}

	/**
	 * Renders a textured model to the screen. Shaders are required to apply colors and using a textured model simply adds a sampler2D uniform to the shaders.
	 * 
	 * @param model
	 *            The model to render
	 */
	public static void render(IModelRenderable model) {
		Renderer.render(model);
	}

	/**
	 * Starts and loads the default transformations and camera position.
	 * 
	 * @param transformationMatrix
	 *            The position of the object
	 * @param camera
	 *            The camera object
	 */
	public static void loadDefaultShader(Matrix4f transformationMatrix, Camera camera) {
		defaultShader.start();
		defaultShader.loadTransformationMatrix(transformationMatrix);
		defaultShader.loadViewMatrix(camera);
	}

	/**
	 * Stops the default shader from running.
	 */
	public static void stopDefaultShader() {
		defaultShader.stop();
	}

	/**
	 * Cleans up the shaders.
	 */
	public static void cleanUp() {
		defaultShader.cleanUp();
	}
}