package com.ocelot.zerra.gfx.shaders;

import org.lwjgl.util.vector.Matrix4f;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Forces a shader to have the transform property.
 * 
 * @author Ocelot5836
 */
public interface ITransformShader {

	/**
	 * Loads the specified matrix to the shaders.
	 * 
	 * @param matrix
	 *            The matrix to load
	 */
	void loadTransformationMatrix(Matrix4f matrix);
}