package com.ocelot.zerra.gfx.shaders;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix2f;
import org.lwjgl.util.vector.Matrix3f;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.ocelot.zerra.Zerra;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * The root for any kind of shader. Handles all of the gl stuff and allows the user to load objects to the shaders form java code.
 * 
 * @author Ocelot5836
 */
public abstract class ShaderProgram {

	private int programID;
	private int vertexShaderID;
	private int fragmentShaderID;

	private static FloatBuffer matrixBuffer2 = BufferUtils.createFloatBuffer(8);
	private static FloatBuffer matrixBuffer3 = BufferUtils.createFloatBuffer(12);
	private static FloatBuffer matrixBuffer4 = BufferUtils.createFloatBuffer(16);

	/**
	 * The default implementation of a shader program containing what any shader program needs.
	 * 
	 * @param baseName
	 *            Takes the name and looks for files named baseName_vertex.glsl and baseName_fragment.glsl. The folder these are also searched for is res/shaders.
	 */
	public ShaderProgram(String baseName) {
		this("shaders/" + baseName + "_vertex", "shaders/" + baseName + "_fragment");
	}

	/**
	 * The default implementation of a shader program containing what any shader program needs.
	 * 
	 * @param vertexFile
	 *            The vertex shader file location found as /vertexFile.glsl
	 * @param fragmentFile
	 *            The fragment shader file location found as /fragmentFile.glsl
	 */
	public ShaderProgram(String vertexFile, String fragmentFile) {
		vertexShaderID = loadShader("/" + vertexFile + ".glsl", GL20.GL_VERTEX_SHADER);
		fragmentShaderID = loadShader("/" + fragmentFile + ".glsl", GL20.GL_FRAGMENT_SHADER);
		programID = GL20.glCreateProgram();
		GL20.glAttachShader(programID, vertexShaderID);
		GL20.glAttachShader(programID, fragmentShaderID);
		bindAttributes();
		GL20.glLinkProgram(programID);
		GL20.glValidateProgram(programID);
		getAllUniformLocations();
	}

	/**
	 * Binds all the attributes to the shader files.
	 */
	protected abstract void bindAttributes();

	/**
	 * Sets all the uniform location integers to the locations of the variables in the shaders.
	 */
	protected abstract void getAllUniformLocations();

	protected void bindAttribute(int attribute, String variableName) {
		GL20.glBindAttribLocation(programID, attribute, variableName);
	}

	protected int getUniformLocation(String uniformName) {
		return GL20.glGetUniformLocation(programID, uniformName);
	}

	protected void loadFloat(int location, float value) {
		GL20.glUniform1f(location, value);
	}

	protected void loadInt(int location, int value) {
		GL20.glUniform1i(location, value);
	}

	protected void loadBoolean(int location, boolean value) {
		GL20.glUniform1f(location, value ? 1 : 0);
	}

	protected void loadVector(int location, Vector2f vector) {
		GL20.glUniform2f(location, vector.x, vector.y);
	}

	protected void loadVector(int location, Vector3f vector) {
		GL20.glUniform3f(location, vector.x, vector.y, vector.z);
	}

	protected void loadVector(int location, Vector4f vector) {
		GL20.glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
	}

	protected void loadMatrix(int location, Matrix2f matrix) {
		matrix.store(matrixBuffer2);
		matrixBuffer2.flip();
		GL20.glUniformMatrix4(location, false, matrixBuffer2);
	}

	protected void loadMatrix(int location, Matrix3f matrix) {
		matrix.store(matrixBuffer3);
		matrixBuffer3.flip();
		GL20.glUniformMatrix4(location, false, matrixBuffer3);
	}

	protected void loadMatrix(int location, Matrix4f matrix) {
		matrix.store(matrixBuffer4);
		matrixBuffer4.flip();
		GL20.glUniformMatrix4(location, false, matrixBuffer4);
	}

	public void start() {
		GL20.glUseProgram(programID);
	}

	public void stop() {
		GL20.glUseProgram(0);
	}

	public void cleanUp() {
		stop();
		GL20.glDetachShader(programID, vertexShaderID);
		GL20.glDetachShader(programID, fragmentShaderID);
		GL20.glDeleteShader(vertexShaderID);
		GL20.glDeleteShader(fragmentShaderID);
		GL20.glDeleteProgram(programID);
	}

	private int loadShader(String file, int type) {
		StringBuilder shaderSource = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(file)));
			String line;
			while ((line = reader.readLine()) != null) {
				shaderSource.append(line).append("//\n");
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			Zerra.getLogger().severe("Could not load shader " + file);
		}
		int shaderID = GL20.glCreateShader(type);
		GL20.glShaderSource(shaderID, shaderSource);
		GL20.glCompileShader(shaderID);
		if (GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
			System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
			System.err.println("Could not compile shader!");
			System.exit(-1);
		}
		return shaderID;
	}
}