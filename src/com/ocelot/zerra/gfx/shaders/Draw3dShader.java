package com.ocelot.zerra.gfx.shaders;

import org.lwjgl.util.vector.Matrix4f;

import com.ocelot.zerra.entities.Camera;
import com.ocelot.zerra.util.Maths;

public class Draw3dShader extends ShaderProgram implements ITransformShader {

	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;

	public Draw3dShader() {
		super("draw3d");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");
	}

	@Override
	public void loadTransformationMatrix(Matrix4f transformation) {
		super.loadMatrix(location_transformationMatrix, transformation);
	}

	public void loadProjectionMatrix(Matrix4f projection) {
		super.loadMatrix(location_projectionMatrix, projection);
	}

	public void loadViewMatrix(Camera camera) {
		super.loadMatrix(location_viewMatrix, Maths.createViewMatrix(camera));
	}
}