package com.ocelot.zerra.gfx.shaders;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector4f;

import com.ocelot.zerra.entities.Camera;
import com.ocelot.zerra.util.Maths;

public class DrawTexturedRectShader extends ShaderProgram implements ITransformShader {

	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_size;
	private int location_textureCoords;

	public DrawTexturedRectShader() {
		super("quads/drawTexturedQuad");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoords");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");
		location_size = super.getUniformLocation("size");
		location_textureCoords = super.getUniformLocation("textureCoords");
	}

	@Override
	public void loadTransformationMatrix(Matrix4f transformation) {
		super.loadMatrix(location_transformationMatrix, transformation);
	}

	public void loadProjectionMatrix(Matrix4f projection) {
		super.loadMatrix(location_projectionMatrix, projection);
	}

	public void loadViewMatrix(Camera camera) {
		super.loadMatrix(location_viewMatrix, Maths.createViewMatrix(camera));
	}

	public void loadSize(float x, float y, float width, float height) {
		super.loadVector(location_size, new Vector4f(x, y, width, height));
	}

	public void loadTextureCoords(float u, float v, float textureWidth, float textureHeight) {
		super.loadVector(location_textureCoords, new Vector4f(u, v, textureWidth, textureHeight));
	}
}