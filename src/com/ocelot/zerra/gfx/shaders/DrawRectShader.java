package com.ocelot.zerra.gfx.shaders;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import com.ocelot.zerra.entities.Camera;
import com.ocelot.zerra.util.Maths;

public class DrawRectShader extends ShaderProgram implements ITransformShader {

	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_size;
	private int location_color1;
	private int location_color2;

	public DrawRectShader() {
		super("quads/drawQuad");
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");
		location_size = super.getUniformLocation("size");
		location_color1 = super.getUniformLocation("color1");
		location_color2 = super.getUniformLocation("color2");
	}

	@Override
	public void loadTransformationMatrix(Matrix4f transformation) {
		super.loadMatrix(location_transformationMatrix, transformation);
	}

	public void loadProjectionMatrix(Matrix4f projection) {
		super.loadMatrix(location_projectionMatrix, projection);
	}

	public void loadViewMatrix(Camera camera) {
		super.loadMatrix(location_viewMatrix, Maths.createViewMatrix(camera));
	}

	public void loadSize(float x, float y, float width, float height) {
		super.loadVector(location_size, new Vector4f(x, y, width, height));
	}

	public void loadColors(int color1, int color2) {
		this.loadColor(location_color1, color1);
		this.loadColor(location_color2, color2);
	}

	public void loadColors(Vector3f color1, Vector3f color2) {
		super.loadVector(location_color1, color1);
		super.loadVector(location_color2, color2);
	}

	private void loadColor(int location, int color) {
		float red = (color) & 0xFF;
		float green = (color >> 8) & 0xFF;
		float blue = (color >> 16) & 0xFF;
		// float alpha = (color >> 24) & 0xFF;
		super.loadVector(location, new Vector3f(red / 255, green / 255, blue / 255));
	}
}