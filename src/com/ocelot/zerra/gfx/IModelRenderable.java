package com.ocelot.zerra.gfx;

import com.ocelot.zerra.models.RawModel;
import com.ocelot.zerra.models.TexturedModel;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Allows an object to be rendered.
 * 
 * @author Ocelot5836
 */
public interface IModelRenderable {

	/**
	 * Only use if you wish to simply render a raw model. Return null if you do not wish to use it.
	 * 
	 * @return The raw model to render
	 */
	RawModel getRawModel();

	/**
	 * Only use if you wish to render a model with a texture. Return null if you do not wish to use it.
	 * 
	 * @return The model to render
	 */
	TexturedModel getModel();

}