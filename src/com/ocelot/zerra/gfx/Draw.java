package com.ocelot.zerra.gfx;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.util.vector.Matrix4f;

import com.ocelot.zerra.Zerra;
import com.ocelot.zerra.gfx.shaders.DrawRectShader;
import com.ocelot.zerra.gfx.shaders.DrawTexturedRectShader;
import com.ocelot.zerra.models.RawModel;
import com.ocelot.zerra.models.TexturedModel;
import com.ocelot.zerra.util.Loader;
import com.ocelot.zerra.util.Maths;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Draws 2D objects to the screen. Used to make more complex OpenGL stuff easier.
 * 
 * @author Ocelot5836
 */
public class Draw {

	/** Stores all translations so they do not need to be created over and over. */
	private static Map<String, Matrix4f> translations = new HashMap<String, Matrix4f>();
	/** The default rectangle model. */
	private static RawModel defaultRect = Loader.loadToVAO(new float[] { 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0 }, new float[] { 0, 1, 1, 1, 0, 0, 1, 0 }, new int[] { 0, 2, 1, 1, 3, 2 });

	/** The shaders used in default rendering. */

	private static DrawRectShader drawRectShader = new DrawRectShader();
	private static DrawTexturedRectShader drawTexturedRectShader = new DrawTexturedRectShader();

	/**
	 * Initalizes the renderer.
	 */
	public static void init() {
		Matrix4f viewMatrix = Renderer.getProjectionMatrix();
		drawRectShader.start();
		drawRectShader.loadProjectionMatrix(viewMatrix);
		drawRectShader.stop();
		drawTexturedRectShader.start();
		drawTexturedRectShader.loadProjectionMatrix(viewMatrix);
		drawTexturedRectShader.stop();
	}

	/**
	 * Draws a rectangle at the specified coords using the supplied width and height in the color that is currently bound.
	 * 
	 * @param x
	 *            The x position to render at
	 * @param y
	 *            The y position to render at
	 * @param width
	 *            The width of the rectangle
	 * @param height
	 *            The height of the rectangle
	 * @param color
	 *            The color of the rectangle
	 */
	public static void drawRect(float x, float y, float width, float height, int color) {
		drawGradientRect(x, y, width, height, color, color);
	}

	/**
	 * Renders a rectangle with a gradient from the top left to the bottom right with the two colors.
	 * 
	 * @param x
	 *            The x position to render at
	 * @param y
	 *            The y position to render at
	 * @param width
	 *            The width of the rectangle
	 * @param height
	 *            The height of the rectangle
	 * @param color1
	 *            The color of the top left
	 * @param color2
	 *            The color of the bottom right
	 */
	public static void drawGradientRect(float x, float y, float width, float height, int color1, int color2) {
		OpenGL.disableDepth();
		drawRectShader.start();
		drawRectShader.loadTransformationMatrix(getTranslation(x, y, 0, 0, width, height));
		drawRectShader.loadViewMatrix(Zerra.getZerra().camera);
		drawRectShader.loadSize(x, y, width, height);
		drawRectShader.loadColors(color1, color2);
		Renderer.render(defaultRect);
		drawRectShader.stop();
	}

	/**
	 * Draws a rectangle using the currently bound texture in {@link OpenGL}.
	 * 
	 * @param x
	 *            The x position to render at
	 * @param y
	 *            The y position to render at
	 * @param width
	 *            The width of the rectangle
	 * @param height
	 *            The height of the rectangle
	 */
	public static void drawTexturedRect(float x, float y, float width, float height) {
		drawTexturedRect(x, y, width, height, 0, 0, 16, 16);
	}

	/**
	 * Draws a rectangle using the currently bound texture in {@link OpenGL}.
	 * 
	 * @param x
	 *            The x position to render at
	 * @param y
	 *            The y position to render at
	 * @param width
	 *            The width of the rectangle
	 * @param height
	 *            The height of the rectangle
	 */
	public static void drawTexturedRect(float x, float y, float width, float height, float u, float v, float textureWidth, float textureHeight) {
		OpenGL.disableDepth();
		if (OpenGL.boundTexture == null) {
			Zerra.getLogger().warning("Attempted to draw rect with a null texture!");
			return;
		}
		TexturedModel model = new TexturedModel(defaultRect, OpenGL.boundTexture);
		drawTexturedRectShader.start();
		drawTexturedRectShader.loadTransformationMatrix(getTranslation(x, y, 0, 0, width, height));
		drawTexturedRectShader.loadViewMatrix(Zerra.getZerra().camera);
		drawTexturedRectShader.loadSize(x, y, width, height);
		drawTexturedRectShader.loadTextureCoords(u, v, textureWidth, textureHeight);
		Renderer.render(model);
		drawTexturedRectShader.stop();
	}

	/**
	 * Cleans up the shaders.
	 */
	public static void cleanUp() {
		drawRectShader.cleanUp();
		drawTexturedRectShader.cleanUp();
	}

	private static Matrix4f getTranslation(float x, float y, float rotationX, float rotationY, float scaleX, float scaleY) {
		Matrix4f translation = translations.get(x + "," + y + "," + rotationX + "," + rotationY + "," + scaleX + "," + scaleY);
		if (translation == null) {
			translation = Maths.createTransformationMatrix(x, y, rotationX, rotationY, scaleX, scaleY);
			translations.put(x + "," + y + "," + rotationX + "," + rotationY + "," + scaleX + "," + scaleY, translation);
		}
		return translation;
	}
}