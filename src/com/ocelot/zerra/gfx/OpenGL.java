package com.ocelot.zerra.gfx;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import com.ocelot.zerra.gfx.textures.ModelTexture;
import com.ocelot.zerra.util.Loader;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Handles the basic OpenGL stuff that involves rendering such as textures and colors.
 * 
 * @author Ocelot5836
 */
public class OpenGL {

	/** Stores all textures so they do not need to be created over and over. */
	private static Map<String, ModelTexture> textures = new HashMap<String, ModelTexture>();

	/**
	 * The currently bound color that is being used to render with.
	 * 
	 * @deprecated This is not a good way to render with custom colors. It is very similar to the limited pipeline OpenGL already had before this.
	 */
	static Vector3f boundColor = new Vector3f();
	/** The currently bound texture used to render with. */
	static ModelTexture boundTexture;

	/**
	 * Sets the current color to use for rendering.
	 * 
	 * @param red
	 *            The red color
	 * @param green
	 *            The green color
	 * @param blue
	 *            The blue color
	 * @deprecated I should not use this as it is fixed
	 */
	public static void color(float red, float green, float blue) {
		boundColor.x = red;
		boundColor.y = green;
		boundColor.z = blue;
	}

	/**
	 * Enables depth in OpenGL.
	 */
	public static void enableDepth() {
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	/**
	 * Disables depth in OpenGL.
	 */
	public static void disableDepth() {
		GL11.glDisable(GL11.GL_DEPTH_TEST);
	}

	/**
	 * Specifies the comparison that takes place during the depth buffer test.
	 * 
	 * @param function
	 *            The new depth parameter
	 * @see GL11#glDepthFunc(int)
	 */
	public static void depthFunc(int function) {
		GL11.glDepthFunc(function);
	}

	/**
	 * Binds a texture that will be used to render with.
	 * 
	 * @param texture
	 *            The texture being bound
	 */
	public static void bindTexture(String texture) {
		ModelTexture newTexture = textures.get(texture);
		if (newTexture == null) {
			newTexture = Loader.loadTexture(texture);
			textures.put(texture, newTexture);
		}
		OpenGL.boundTexture = newTexture;
	}
}