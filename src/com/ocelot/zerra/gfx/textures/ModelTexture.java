package com.ocelot.zerra.gfx.textures;

public class ModelTexture {

	private int textureID;

	public ModelTexture(int textureID) {
		this.textureID = textureID;
	}

	public int getId() {
		return textureID;
	}
}