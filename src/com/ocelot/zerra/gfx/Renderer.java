package com.ocelot.zerra.gfx;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import com.ocelot.zerra.models.RawModel;
import com.ocelot.zerra.models.TexturedModel;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Has the basic capability to render things.
 * 
 * @author Ocelot5836
 */
public class Renderer {

	public static final float FOV = 70;
	public static final float NEAR_PLANE = 0.1f;
	public static final float FAR_PLANE = 1000;

	private static Matrix4f projectionMatrix;

	/**
	 * Renders a model to the screen.
	 * 
	 * @param renderable
	 *            The rendering to render
	 */
	public static void render(IModelRenderable renderable) {
		if (renderable.getRawModel() != null) {
			render(renderable.getRawModel());
		} else if (renderable.getModel() != null) {
			render(renderable.getModel());
		}
	}

	/**
	 * Renders a raw model to the screen.
	 * 
	 * @param model
	 *            The model to render
	 */
	public static void render(RawModel model) {
		GL30.glBindVertexArray(model.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL11.glDrawElements(GL11.GL_TRIANGLES, model.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
	}

	/**
	 * Renders a model with a texture loaded to the screen.
	 * 
	 * @param model
	 *            The textured model to render
	 */
	public static void render(TexturedModel model) {
		RawModel rawModel = model.getRawModel();
		GL30.glBindVertexArray(rawModel.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.getTexture().getId());
		GL11.glDrawElements(GL11.GL_TRIANGLES, rawModel.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
		GL11.glBindTexture(GL13.GL_TEXTURE0, 0);
		GL20.glDisableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL30.glBindVertexArray(0);
	}

	private static Matrix4f createProjectionMatrix() {
		float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
		float y_scale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))) * aspectRatio);
		float x_scale = y_scale / aspectRatio;
		float frustum_length = FAR_PLANE - NEAR_PLANE;

		Matrix4f matrix = new Matrix4f();
		matrix.m00 = x_scale;
		matrix.m11 = y_scale;
		matrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
		matrix.m23 = -1;
		matrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
		matrix.m33 = 0;
		return matrix;
	}

	/**
	 * @return The matrix used to add depth the the game
	 */
	public static Matrix4f getProjectionMatrix() {
		return projectionMatrix == null ? projectionMatrix = createProjectionMatrix() : projectionMatrix;
	}
}