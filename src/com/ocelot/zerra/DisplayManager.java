package com.ocelot.zerra;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.PixelFormat;

import com.ocelot.utils.Logger;
import com.ocelot.utils.Logging;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * Manages the Display for version 2. Needs to be redone in version 3.
 * 
 * @author Ocelot5836
 */
public class DisplayManager {

	private static final Logger LOGGER = Logging.getLogger("Display");

	/**
	 * Creates a new display with the specified width and height.
	 * 
	 * @param width
	 *            The width of the window
	 * @param height
	 *            The height of the window
	 */
	public static void createDisplay(int width, int height) {
		createDisplay("Window", width, height);
	}

	/**
	 * Creates a new display with the specified title, width, and height.
	 * 
	 * @param title
	 *            The title of the window
	 * @param width
	 *            The width of the window
	 * @param height
	 *            The height of the window
	 */
	public static void createDisplay(String title, int width, int height) {
		ContextAttribs attributes = new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true);

		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.create(new PixelFormat().withSamples(4), attributes);
			Display.setTitle(title);
			GL11.glEnable(GL13.GL_MULTISAMPLE);
		} catch (LWJGLException e) {
			e.printStackTrace();
			LOGGER.severe("Could not create display " + title + ":" + width + "x" + height);
		}

		GL11.glViewport(0, 0, width, height);
	}

	/**
	 * Updates the display.
	 */
	public static void updateDisplay() {
		Display.sync(120);
		Display.update();
	}

	/**
	 * Closes the display.
	 */
	public static void closeDisplay() {
		Display.destroy();
	}
}