package com.ocelot.zerra;

import java.io.IOException;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import com.ocelot.utils.Logger;
import com.ocelot.utils.Logging;
import com.ocelot.zerra.entities.Camera;
import com.ocelot.zerra.gfx.Draw;
import com.ocelot.zerra.gfx.Draw3D;
import com.ocelot.zerra.gfx.OpenGL;
import com.ocelot.zerra.util.Loader;
import com.ocelot.zerra.util.Timer;

/**
 * <em><b>Copyright (c) 2018 Ocelot5836.</b></em>
 * 
 * <br>
 * </br>
 * 
 * The main Zerra class. Renders and updates the game as well as handles it all.
 * 
 * @author Ocelot5836
 * @author Hypeirochus
 */
public class Zerra {

	public static final String TITLE = "Zerra";

	private static Zerra instance;
	private static Logger logger;

	private final Timer timer = new Timer(20.0F);
	public int displayWidth;
	public int displayHeight;
	public Camera camera;

	private static boolean isJvm64Bit = GLCheckers.isJvm64bit();

	volatile boolean running = true;

	public Zerra() {
		this.displayWidth = 1280;
		this.displayHeight = 720;
	}

	public void run() {
		this.running = true;

		try {
			this.init();
		} catch (Exception e) {
			e.printStackTrace();
		}

		while (true) {
			try {
				while (this.running) {
					DisplayManager.updateDisplay();
					GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
					if (Display.isCreated() && Display.isCloseRequested())
						this.running = false;
					this.timer.updateTimer();

					for (int j = 0; j < Math.min(10, this.timer.elapsedTicks); ++j) {
						this.update();
					}

					this.render();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return;
		}
	}

	private void init() throws LWJGLException, IOException {
		logger.info("LWJGL Version: " + (Object) Sys.getVersion());
		DisplayManager.createDisplay(TITLE, this.displayWidth, this.displayHeight);
		Display.setResizable(false);
		GLCheckers.setWindowIcon();
		Keyboard.create();
		Mouse.create();

		Draw.init();
		Draw3D.init();

		camera = new Camera(0, 0, 1.5f);

		GL11.glClearColor(0.1f, 0.5f, 0.1f, 1);
		GLCheckers.checkGLError("Pre-Init");
	}

	private void update() {
		camera.update();
	}

	private void render() {
		Draw.drawRect(-0.5f, -0.5f, 1, 1, 0x7f007f);
		OpenGL.bindTexture("grass");
		Draw.drawTexturedRect(-0.25f, -0.4f, 0.5f, 0.5f);
		Draw.drawGradientRect(-0.8f, -0.4f, 0.5f, 0.5f, 0xff00ff, 0x7f007f);
	}

	private void cleanUp() {
		Loader.cleanUp();
		Draw.cleanUp();
		Draw3D.cleanUp();
	}

	public void stop() {
		logger.info("Stopping...");
		cleanUp();
		DisplayManager.closeDisplay();
	}

	/**
	 * Gets the system time in milliseconds.
	 */
	public static long getSystemTime() {
		return Sys.getTime() * 1000L / Sys.getTimerResolution();
	}

	/**
	 * @return Whether or not the jvm is 64 bit.
	 */
	public static boolean isJvm64Bit() {
		return isJvm64Bit;
	}

	/**
	 * @return The Zerra instance
	 */
	public static Zerra getZerra() {
		return instance == null ? instance = new Zerra() : instance;
	}

	/**
	 * @return The zerra logger
	 */
	public static Logger getLogger() {
		return logger == null ? logger = Logging.getLogger(TITLE) : logger;
	}
}