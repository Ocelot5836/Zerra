package com.ocelot.zerra.entities;

import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector3f;

public class Camera {

	private Vector3f position;
	private float pitch;
	private float yaw;
	private float roll;

	public Camera() {
		this(0, 0, 0, 0, 0, 0);
	}

	public Camera(float x, float y, float z) {
		this(x, y, z, 0, 0, 0);
	}

	public Camera(Vector3f position) {
		this(position.x, position.y, position.z, 0, 0, 0);
	}

	public Camera(Vector3f position, float pitch, float yaw, float roll) {
		this(position.x, position.y, position.z, pitch, yaw, roll);
	}

	public Camera(float x, float y, float z, float pitch, float yaw, float roll) {
		this.position = new Vector3f(x, y, z);
		this.pitch = pitch;
		this.yaw = yaw;
		this.roll = roll;
	}

	public void update() {
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			position.z -= 0.04f;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			position.z += 0.04f;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
			position.y += 0.04f;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			position.y -= 0.04f;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			position.x -= 0.04f;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			position.x += 0.04f;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			yaw -= 4;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			yaw += 4;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			pitch -= 4;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			pitch += 4;
		}
	}

	public Vector3f getPosition() {
		return position;
	}

	public float getPitch() {
		return pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public float getRoll() {
		return roll;
	}

	public Camera setPosition(Vector3f position) {
		this.position.x = position.x;
		this.position.y = position.y;
		this.position.z = position.z;
		return this;
	}

	public Camera setPosition(float x, float y, float z) {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
		return this;
	}

	public Camera setPitch(float pitch) {
		this.pitch = pitch;
		return this;
	}

	public Camera setYaw(float yaw) {
		this.yaw = yaw;
		return this;
	}

	public Camera setRoll(float roll) {
		this.roll = roll;
		return this;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "@" + position + "," + yaw + "," + pitch + "," + roll;
	}
}