package com.ocelot.zerra.entities;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import com.ocelot.zerra.Zerra;
import com.ocelot.zerra.gfx.IModelRenderable;
import com.ocelot.zerra.gfx.Renderer;

public abstract class Entity implements IModelRenderable {

	private Vector3f position;
	private Vector3f rotation;
	private Vector3f scale;

	public Entity(float x, float y, float rotationX, float rotationY, float scaleX, float scaleY) {
		this(new Vector2f(x, y), new Vector2f(rotationX, rotationY), new Vector2f(scaleX, scaleY));
	}

	public Entity(Vector2f position, Vector2f rotation, Vector2f scale) {
		this.position = new Vector3f(position.x, position.y, 0);
		this.rotation = new Vector3f(rotation.x, rotation.y, 0);
		this.scale = new Vector3f(scale.x, scale.y, 0);
	}

	public void update() {
	}

	public void render(Zerra zerra) {
		Renderer.render(this);
	}

	public void increasePosition(float dx, float dy) {
		this.position.x += dx;
		this.position.y += dy;
	}

	public void increaseRotation(float dx, float dy) {
		this.rotation.x += dx;
		this.rotation.y += dy;
	}

	public void decreasePosition(float dx, float dy) {
		this.increasePosition(-dx, -dy);
	}

	public void decreaseRotation(float dx, float dy) {
		this.increaseRotation(-dx, -dy);
	}

	public Vector3f getPosition() {
		return position;
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public Vector3f getScale() {
		return scale;
	}

	public void setPosition(Vector2f position) {
		this.position.x = position.x;
		this.position.y = position.y;
	}

	public void setRotation(Vector2f rotation) {
		this.rotation.x = rotation.x;
		this.rotation.y = rotation.y;
	}

	public void setScale(Vector2f scale) {
		this.scale.x = scale.x;
		this.scale.y = scale.y;
	}
}