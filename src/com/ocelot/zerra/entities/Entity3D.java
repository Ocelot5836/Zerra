package com.ocelot.zerra.entities;

import org.lwjgl.util.vector.Vector3f;

import com.ocelot.zerra.Zerra;
import com.ocelot.zerra.gfx.IModelRenderable;
import com.ocelot.zerra.gfx.Renderer;

public abstract class Entity3D implements IModelRenderable {

	private Vector3f position;
	private Vector3f rotation;
	private Vector3f scale;

	public Entity3D(float x, float y, float z, float rotationX, float rotationY, float rotationZ, float scaleX, float scaleY, float scaleZ) {
		this(new Vector3f(x, y, z), new Vector3f(rotationX, rotationY, rotationZ), new Vector3f(scaleX, scaleY, scaleZ));
	}

	public Entity3D(Vector3f position, Vector3f rotation, Vector3f scale) {
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
	}

	public void update() {
	}

	public void render(Zerra zerra) {
		Renderer.render(this);
	}

	public void increasePosition(float dx, float dy, float dz) {
		this.position.x += dx;
		this.position.y += dy;
		this.position.z += dz;
	}

	public void increaseRotation(float dx, float dy, float dz) {
		this.rotation.x += dx;
		this.rotation.y += dy;
		this.rotation.z += dz;
	}

	public void decreasePosition(float dx, float dy, float dz) {
		this.increasePosition(-dx, -dy, -dz);
	}

	public void decreaseRotation(float dx, float dy, float dz) {
		this.increaseRotation(-dx, -dy, -dz);
	}

	public Vector3f getPosition() {
		return position;
	}

	public Vector3f getRotation() {
		return rotation;
	}

	public Vector3f getScale() {
		return scale;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public void setRotation(Vector3f rotation) {
		this.rotation = rotation;
	}

	public void setScale(Vector3f scale) {
		this.scale = scale;
	}
}