package com.ocelot.zerra;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;

import com.ocelot.utils.Logger;

public class GLCheckers {

	private static final Logger LOGGER = Zerra.getLogger();

	/**
	 * Creates the display.
	 */
	public static void createDisplay() throws LWJGLException {
		Display.setResizable(false);
		Display.setTitle("Zerra");

		try {
			Display.create((new PixelFormat()).withDepthBits(24));
		} catch (LWJGLException e) {
			// LOGGER.error("Couldn't set pixel format", (Throwable) e);
			LOGGER.warning("Couldn't set pixel format");
			e.printStackTrace();

			try {
				Thread.sleep(1000L);
			} catch (InterruptedException ie) {
			}

			// if (this.fullscreen)
			// {
			// this.updateDisplayMode();
			// }

			Display.create();
		}
	}

	/**
	 * Sets the window's icon.
	 */
	public static void setWindowIcon() {
		/** An example from Minecraft to be used later */
		// Util.EnumOS util$enumos = Util.getOSType();
		//
		// if (util$enumos != Util.EnumOS.OSX) {
		// InputStream inputstream = null;
		// InputStream inputstream1 = null;
		//
		// try {
		// inputstream = this.mcDefaultResourcePack.getInputStreamAssets(new ResourceLocation("icons/icon_16x16.png"));
		// inputstream1 = this.mcDefaultResourcePack.getInputStreamAssets(new ResourceLocation("icons/icon_32x32.png"));
		//
		// if (inputstream != null && inputstream1 != null) {
		// Display.setIcon(new ByteBuffer[] { this.readImageToBuffer(inputstream), this.readImageToBuffer(inputstream1) });
		// }
		// } catch (IOException ioexception) {
		// LOGGER.error("Couldn't set icon", (Throwable) ioexception);
		// } finally {
		// IOUtils.closeQuietly(inputstream);
		// IOUtils.closeQuietly(inputstream1);
		// }
		// }
	}

	/**
	 * @return Whether or not the JVM is 64 bit or not
	 */
	public static boolean isJvm64bit() {
		String[] astring = new String[] { "sun.arch.data.model", "com.ibm.vm.bitmode", "os.arch" };

		for (String s : astring) {
			String s1 = System.getProperty(s);

			if (s1 != null && s1.contains("64")) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks for an OpenGL error. If there is one, prints the error ID and error string.
	 * 
	 * @param message
	 *            The message to print
	 */
	public static void checkGLError(String message) {
		int i = GL11.glGetError();

		if (i != 0) {
			String s = GLU.gluErrorString(i);
			LOGGER.warning("########## GL ERROR ##########");
			LOGGER.warning("@ " + (Object) message);
			LOGGER.warning(Integer.valueOf(i) + ": " + s);
		}
	}
}