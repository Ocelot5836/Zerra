package com.ocelot.zerra.start;

import com.ocelot.zerra.Zerra;

public class Start {

	private static Zerra zerra = Zerra.getZerra();

	public static void main(String[] args) {
		new Thread(Zerra.TITLE).start();
		zerra.run();
	}
}